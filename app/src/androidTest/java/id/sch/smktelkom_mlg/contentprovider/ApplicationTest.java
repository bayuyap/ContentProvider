package id.sch.smktelkom_mlg.contentprovider;

/**
 * Created by SMK TELKOM on 3/10/2018.
 */

import android.app.Application;
import android.test.ApplicationTestCase;

public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }
}
